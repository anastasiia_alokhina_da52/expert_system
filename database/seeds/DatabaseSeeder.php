<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $recipes = require __DIR__ . '/../recipes.php';

        $dishes = collect($recipes)->transform(function($item){
            return ['name' => $item[0]];
        })->unique('name')->values();

        $products = collect($recipes)->transform(function($item){
            return ['name' => $item[1]];
        })->unique('name')->values();

        $productsDishes = collect($recipes)->transform(function($item) use ($dishes, $products) {
            return [
                'dish_id' => $dishes->search(['name' => $item[0]]) + 1,
                'product_id' => $products->search(['name' => $item[1]]) + 1,
            ];
        });

        DB::table('dishes')->insert($dishes->all());
        DB::table('products')->insert($products->all());
        DB::table('products_dishes')->insert($productsDishes->all());
    }
}
