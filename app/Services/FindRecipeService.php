<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;

class FindRecipeService
{
    protected $products;
    protected $extraProducts;
    protected $alwaysHomeProducts = ['water', 'pepper', 'salt'];
    protected $availableProductsIds;
    protected $possibleDishesIds;
    protected $fullDishes;
    protected $partlyDishes;

    public function __construct(array $products = [])
    {
        $this->products = $products;
        $this->findPossibleDishesIds();
        $this->findFullDishes();
        $this->findPartlyDishes();
    }

    public function getFullDishes()
    {
        return $this->fullDishes;
    }

    public function getPartlyDishes()
    {
        return $this->partlyDishes;
    }

    public function getExtraProducts()
    {
        return $this->extraProducts;
    }

    protected function findFullDishes()
    {
        $availableProductsIds = $this->availableProductsIds;
        $fullDishesIds = $this->possibleDishesIds
            ->filter(function($dish) use ($availableProductsIds){
                $allProductsIds = (DB::table('products_dishes')
                    ->select('product_id')
                    ->where('dish_id', '=', $dish)
                    ->get()
                    ->pluck('product_id'));
                $neededProductsIds = $allProductsIds->diff($this->availableProductsIds);
                return (count($neededProductsIds) === 0);
            });
        $this->fullDishes = DB::table('dishes')
            ->select('name', 'id')
            ->whereIn('id', $fullDishesIds->all())
            ->get()
            ->all();
    }

    protected function findPartlyDishes()
    {
        $availableProductsIds = $this->availableProductsIds;
        $partlyDishesIds = $this->possibleDishesIds
            ->filter(function($dish) use ($availableProductsIds){
                $allProductsIds = (DB::table('products_dishes')
                    ->select('product_id')
                    ->where('dish_id', '=', $dish)
                    ->get()
                    ->pluck('product_id'));
                $neededProductsIds = $allProductsIds->diff($this->availableProductsIds);
                return (count($neededProductsIds) <= count($allProductsIds)*0.4) && (count($neededProductsIds) !== 0);
            });
        $this->partlyDishes = DB::table('dishes')
            ->select('name', 'id')
            ->whereIn('id', $partlyDishesIds->all())
            ->get()
            ->transform(function($dish) use ($availableProductsIds){
                $neededProducts = (DB::table('products_dishes')
                    ->select('product_id', 'products.name')
                    ->where('dish_id', '=', $dish->id)
                    ->whereNotIn('product_id', $this->availableProductsIds)
                    ->join('products', 'products_dishes.product_id', '=', 'products.id')
                    ->get()
                    ->all());
                $dish->need = $neededProducts;
                return $dish;
            })
            ->all();
        $this->extraProducts = collect($this->partlyDishes)->pluck('need')->collapse()->pluck('name')->unique()->all();
    }

    protected function findAvailableProductsIds()
    {
        $availableProducts = array_merge($this->products, $this->alwaysHomeProducts);
        $this->availableProductsIds = DB::table('products')
            ->select('id')
            ->whereIn('name', $availableProducts)
            ->get()
            ->pluck('id');
    }

    protected function findPossibleDishesIds()
    {
        $this->findAvailableProductsIds();
        $this->possibleDishesIds = DB::table('products_dishes')
            ->select('dish_id')
            ->whereIn('product_id', $this->availableProductsIds->all())
            ->groupBy('dish_id')
            ->get()
            ->pluck('dish_id');
    }
}