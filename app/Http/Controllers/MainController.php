<?php

namespace App\Http\Controllers;

use App\Services\FindRecipeService;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Request $request)
    {
        $request->session()->forget('all');
        return view('index');
    }

    public function find(Request $request)
    {
        $request->session()->put('all', $request->products);
        $wantToUseProducts = explode(', ', $request->products);
        $service = new FindRecipeService($wantToUseProducts);
        $fullDishes = $service->getFullDishes();
        $partlyDishes = $service->getPartlyDishes();
        $extraProducts = $service->getExtraProducts();
        return view('dishes', ['fullDishes' => $fullDishes, 'partlyDishes' => $partlyDishes, 'extraProducts' => $extraProducts]);
    }

    public function extra(Request $request)
    {
        $extraProducts = $request->extra;
        $previousProducts = $request->session()->get('all');
        $request->session()->put('all', $previousProducts . ', ' . implode(', ', $extraProducts));
        $allProducts = array_merge(explode(', ', $previousProducts), $extraProducts);
        $service = new FindRecipeService($allProducts);
        $fullDishes = $service->getFullDishes();
        $partlyDishes = $service->getPartlyDishes();
        $extraProducts = $service->getExtraProducts();
        return view('dishes', ['fullDishes' => $fullDishes, 'partlyDishes' => $partlyDishes, 'extraProducts' => $extraProducts]);

    }
}
