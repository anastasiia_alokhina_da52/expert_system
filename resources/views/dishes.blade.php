<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dishes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="jumbotron">
    <h2 align="center">You have all products for these dishes</h2>
    @foreach($fullDishes as $fullDish)
        @if ($loop->index % 4 === 0)
            <ul class="list-group list-group-horizontal">
        @endif
            <li class="list-group-item flex-fill text-center">{{$fullDish->name}}<br>
                <img src="{{asset("img/recipes/{$fullDish->id}.png")}}" alt="">
            </li>
        @if ($loop->index % 4 === 3 || $loop->last)
            </ul>
        @endif
    @endforeach
    <hr class="my-4">
    <h2 align="center">More recipes with some other products</h2>
    @foreach ($partlyDishes as $partlyDish)
        @if ($loop->index % 4 === 0)
            <ul class="list-group list-group-horizontal">
        @endif
        <li class="list-group-item flex-fill text-center">{{$partlyDish->name}}<br>
            <img src="{{asset("img/recipes/{$partlyDish->id}.png")}}" alt="">
            <ul>
                @foreach ($partlyDish->need as $product)
                    <li class="text-left">{{$product->name}}</li>
                @endforeach
            </ul>
        </li>
        @if ($loop->index % 4 === 3 || $loop->last)
            </ul>
        @endif
    @endforeach
    <br>
    <h3 align="center">Choose which products you have:</h3>
    <div align="center">
        <form action="/extra" method="post">
            @csrf
            @foreach ($extraProducts as $product)
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="extra[]" value="{{$product}}">{{$product}}
                </div>
            @endforeach
            <br>
            <br>
            <button type="submit" class="btn btn-primary btn-block">Find more recipes</button>
        </form>
    </div>
</div>
</body>
</html>