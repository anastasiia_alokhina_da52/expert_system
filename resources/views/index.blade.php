<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Want to cook?</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="jumbotron">
    <h1 align="center">Want to cook?</h1>
    <hr class="my-4">
    <form action="/find" method="post" class="form-group">
        @csrf
        <input type="text" name="products" class="form-control">
        <hr class="my-4">
        <button type="submit" class="btn btn-primary btn-block">Find</button>
    </form>
</div>
</div>
</body>
</html>